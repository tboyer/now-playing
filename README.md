Sample application that pulls the current track metadata from various third-party apps, including
Play Music, Spotify, and more. It taps into the
[iTunes Search API](https://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html)
to pull available album artwork for tracks.

![MainActivity](https://bytebucket.org/tboyer/now-playing/raw/93ede5e56811448675d656ebec486fa462173cf7/MainActivity.png)

## Setup

Clone the project, build, and run! After the app launches, open the music player and play a few
songs. Go back to the app to see your recent plays.