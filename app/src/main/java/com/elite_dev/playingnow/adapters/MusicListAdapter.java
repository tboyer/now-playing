package com.elite_dev.playingnow.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elite_dev.playingnow.R;
import com.elite_dev.playingnow.Song;

import java.util.List;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class MusicListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Song> details;
    private Context mContext;

    public MusicListAdapter(Context context, List<Song> details) {
        this.details = details;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.media_row_item,
                viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Song detail = details.get(i);

        ViewHolder holder = (ViewHolder) viewHolder;

        // If we don't have an album cover to use, use the default one.
        Drawable album = detail.albumArtwork == null ? mContext.getResources()
                .getDrawable(R.drawable.no_album) : detail.albumArtwork;

        holder.albumArtwork.setImageDrawable(album);
        holder.trackTitle.setText(detail.track);
        holder.artistTitle.setText(detail.artist);
    }

    @Override
    public int getItemCount() {
        return details.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView albumArtwork;
        public TextView trackTitle;
        public TextView artistTitle;

        public ViewHolder(View itemView) {
            super(itemView);

            albumArtwork = (ImageView) itemView.findViewById(R.id.album_artwork);
            trackTitle = (TextView) itemView.findViewById(R.id.track_title);
            artistTitle = (TextView) itemView.findViewById(R.id.artist_title);
        }
    }
}