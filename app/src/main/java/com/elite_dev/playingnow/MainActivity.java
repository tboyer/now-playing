package com.elite_dev.playingnow;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.elite_dev.playingnow.adapters.MusicListAdapter;
import com.elite_dev.playingnow.itunes.AlbumLookup;
import com.elite_dev.playingnow.util.ExternalImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class MainActivity extends ActionBarActivity {

    private List<Song> recentlyPlayed;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up the action toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.action_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        // Init a list for recently played TODO These should be initialized from a database/file
        recentlyPlayed = new ArrayList<Song>();

        recyclerView = (RecyclerView) findViewById(R.id.list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        MusicListAdapter adapter = new MusicListAdapter(this, recentlyPlayed);
        recyclerView.setAdapter(adapter);

        registerReceiver(musicReceiver, buildFilter());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    /**
     * Creates a filter used to capture music player metadata from various app sources.
     * Note: this can also be used to capture {@code .playstatechanged}, {@code .playbackcomplete},
     * or {@code .queuechanged}.
     * @return The intent filter for the music player metadata.
     */
    private IntentFilter buildFilter() {
        IntentFilter iF = new IntentFilter();

        iF.addAction("com.android.music.metachanged");
        iF.addAction("com.htc.music.metachanged");
        iF.addAction("fm.last.android.metachanged");
        iF.addAction("com.sec.android.app.music.metachanged");
        iF.addAction("com.nullsoft.winamp.metachanged");
        iF.addAction("com.amazon.mp3.metachanged");
        iF.addAction("com.miui.player.metachanged");
        iF.addAction("com.real.IMP.metachanged");
        iF.addAction("com.sonyericsson.music.metachanged");
        iF.addAction("com.rdio.android.metachanged");
        iF.addAction("com.samsung.sec.android.MusicPlayer.metachanged");
        iF.addAction("com.andrew.apollo.metachanged");

        return iF;
    }

    private BroadcastReceiver musicReceiver;

    {
        musicReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                String cmd = intent.getStringExtra("command");
                Log.v("tag ", action + " / " + cmd);

                final Song song = new Song();

                if (action.equals("com.amazon.mp3.metachanged")) {
                    song.artist = intent.getStringExtra("com.amazon.mp3.artist");
                    song.album = intent.getStringExtra("com.amazon.mp3.album");
                    song.track = intent.getStringExtra("com.amazon.mp3.track");
                } else {
                    song.artist = intent.getStringExtra("artist");
                    song.album = intent.getStringExtra("album");
                    song.track = intent.getStringExtra("track");
                }
                Log.v("tag", song.artist + ":" + song.album + ":" + song.track);

                // Uncomment to see background task working while in media apps
//                Toast.makeText(MainActivity.this, song.artist + ":" + song.album + ":" + song.track,
//                        Toast.LENGTH_LONG).show();

                // Store the song in recently played if it hasn't already been added
                if (recentlyPlayed.size() == 0) {
                    updateRecentlyPlayed(song);
                } else {
                    boolean updateList = true;

                    for (int i = 0; i < recentlyPlayed.size(); i++) {
                        Song recent = recentlyPlayed.get(i);
                        if (recent.track.equalsIgnoreCase(song.track) &&
                                recent.artist.equalsIgnoreCase(song.artist)) {

                            // The song was already added, we don't need to update
                            updateList = false;
                        }
                    }

                    if (updateList) {
                        updateRecentlyPlayed(song);
                    }
                }
            }
        };
    }

    private void updateRecentlyPlayed(Song song) {
        Log.v("tag", "Adding \"" + song.track + "\" to recentlyPlayed");

        // Add a timestamp for the track
        song.timestamp = System.currentTimeMillis();

        getAlbumArtworkForSong(song);

        // Add the song to the list
        recentlyPlayed.add(song);

        // Update the list adapter
        updateListAdapter();
    }

    private void getAlbumArtworkForSong(final Song song) {
        // Pull the album artwork details
        AlbumLookup.getAlbumArtwork(song, new Callback<String>() {
            @Override
            public void success(final String s, Response response) {
                Log.v("tag", s);
                for (final Song recent : recentlyPlayed) {
                    // TODO This is just a basic check - this should probably use an id to find the song
                    if (recent.album.equalsIgnoreCase(song.album)) {
                        new Thread() {
                            public void run() {
                                try {
                                    recent.albumArtwork = new ExternalImage(MainActivity.this, s).drawableFromUrl();
                                    updateListAdapter();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();

                        // We got what we needed, we're done
                        return;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("tag", error.getMessage());
            }
        });
    }

    private void updateListAdapter() {
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.getAdapter().notifyDataSetChanged();
            }
        });
    }

    // Ensure that the app keeps running in the background
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
