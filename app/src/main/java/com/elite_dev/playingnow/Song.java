package com.elite_dev.playingnow;

import android.graphics.drawable.Drawable;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class Song {
    public String track;
    public String artist;
    public String album;
    public Drawable albumArtwork;
    public long timestamp;

    public Song() { }

    public Song(String track, String artist, String album) {
        this.track = track;
        this.album = album;
        this.artist = artist;
    }
}
