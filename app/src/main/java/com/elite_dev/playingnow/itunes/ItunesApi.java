package com.elite_dev.playingnow.itunes;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class ItunesApi {
    public static final String API_URL = "https://itunes.apple.com";

    public interface MusicSearchInterface {
        @GET("/search?media=music")
        void getAlbum(@Query("term") String album, Callback<ItunesResponse> callback);
    }

    public static MusicSearchInterface getAlbumSearchApi() {
        return new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(API_URL).build()
                .create(MusicSearchInterface.class);
    }
}
