package com.elite_dev.playingnow.itunes;

import com.elite_dev.playingnow.Song;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by TrevorBoyer on 1/6/15.
 */
public class AlbumLookup {
    /**
     * Returns the string url location of the album artwork.
     * @param song The song to look album artwork up for.
     * @param callback Callback when the lookup is completed;
     */
    public static void getAlbumArtwork(final Song song, final Callback<String> callback) {
        ItunesApi.getAlbumSearchApi().getAlbum(song.album, new Callback<ItunesResponse>() {
            @Override
            public void success(ItunesResponse itunesResponse, Response response) {
                if (itunesResponse != null) {
                    // Ensure we pull the album for the correct artist
                    for (ItunesResult result : itunesResponse.results) {
                        if (result.artistName.equalsIgnoreCase(song.artist)) {
                            callback.success(result.artworkUrl100, response);
                        }

                        // If the artists match, we're done
                        break;
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                callback.failure(error);
            }
        });
    }
}
