package com.elite_dev.playingnow.itunes;

import java.util.List;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class ItunesResponse {
    private int resultCount;
    public List<ItunesResult> results;
}
