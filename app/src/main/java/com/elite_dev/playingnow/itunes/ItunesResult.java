package com.elite_dev.playingnow.itunes;

/**
 * Created by Trevor Boyer on 1/6/15.
 */
public class ItunesResult {
    public int artistId;
    public int collectionId;
    public int trackId;
    public String artistName;
    public String collectionName; // The album name
    public String trackName;
    public String collectionCensoredName;
    public String trackCensoredName;
    public String artworkUrl100; // 100x100 pixels
}
