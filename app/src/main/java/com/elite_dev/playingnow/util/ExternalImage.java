package com.elite_dev.playingnow.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by TrevorBoyer on 1/6/15.
 */
public class ExternalImage {
    private String url;
    private Context mContext;

    public ExternalImage(Context context, String url) {
        this.mContext = context;
        this.url = url;
    }

    public Drawable drawableFromUrl() throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);

        return new BitmapDrawable(this.mContext.getResources(), x);
    }
}
