package com.elite_dev.playingnow;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import com.elite_dev.playingnow.itunes.AlbumLookup;

import org.junit.Test;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    @Test
    public void testGetAlbumArtwork() throws Exception {
        Song song = new Song("One More Night", "Maroon 5", "Overexposed");

        AlbumLookup.getAlbumArtwork(song, new Callback<String>() {
            @Override
            public void success(final String s, Response response) {
                assertEquals(s, "http://a5.mzstatic.com/us/r30/Music/v4/db/8c/c8/db8cc841-47a0-2845-5116-e1762dbd711e/UMG_cvrart_00602537109692_01_RGB72_1200x1200_12UMGIM26178.100x100-75.jpg");
            }

            @Override
            public void failure(RetrofitError error) {
                Log.v("androidTest", error.getMessage());
            }
        });
    }
}